// NPM IMPORTS
import React, { useEffect, useState } from 'react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import ReactFlagsSelect from 'react-flags-select';
import 'react-flags-select/css/react-flags-select.css';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';

import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'

import {countryList,getCountryName} from "./countryList";


//COMPONENTS IMPORT
import NavBar from "../NavBar/Navbar";

//MEDIA 
import Flag from "../../../media/flag.png"

//STYLES
import { Container, Heading, FormContainer, FormItem, Label, InputField, FormElement, SelectItem, BigButton, Footer, Error, FlagIcon } from "./PersonalDetails.styled";

const PersonalDetails = () =>{
      
    var bool = false;

    const history = useHistory();

    const [number,setNumber] = useState('');

    const[country,setCountry] = useState()
    const [region,setRegion] = useState()

    const [countryCode, setCountryCode] = useState("IN")

    const { register, handleSubmit, errors } = useForm();

    // const handleChange = (event) => {
    //     const re = /^[0-9\b]+$/;
    //       if (event.target.value === '' || re.test(event.target.value)) {
    //          setNumber(event.target.value);
    //       }
    //   }

    const onsubmit = (data) =>{
        if(data){
            if(countryCode && region){
                data.country = countryCode
                data.phone_number = countryList[countryCode].code + "-" + data.phone_number
                data.state = region
            }
            history.push("/company_details");
            localStorage.setItem("isValidPersonalDetails",JSON.stringify(!bool));
            localStorage.setItem("PersonalDetails",JSON.stringify(data));
        }
    }

    useEffect(()=>{
        localStorage.setItem("isValidPersonalDetails",JSON.stringify(bool));
        localStorage.setItem("isValidCompanyDetails",JSON.stringify(bool));
    },[]);

    const selectCountry = (code) =>{
        setCountryCode(code)
    }

    const selectRegion = (code) =>{
        setRegion(code)
    }


    return <>
    <NavBar active="PersonalDetails" completed="" />
    <Container>
        <Heading>
            <h2>Add your personal details</h2>
            <h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3> 
        </Heading>
        <FormContainer method="post" onSubmit={handleSubmit(onsubmit)}>
            <FormItem>
                <Label>Full Name</Label>
                <InputField autoComplete="new-password" type="text" name="full_name" ref={register({required:"This field is required"})} />
                {errors.full_name && <Error>{errors.full_name.message}</Error>}
            </FormItem>
            <FormItem>
                <Label>Gender</Label>
                <FormElement>
                <input type="radio" ref={register({required:"This field is required"})} name="gender" value="male" id="male" defaultChecked/><label htmlFor="male">Male</label>
                <input type="radio" ref={register({required:"This field is required"})} name="gender" value="female" id="female" /><label htmlFor="female">Female</label>
                <input type="radio" ref={register({required:"This field is required"})} name="gender" value="other" id="other" /><label htmlFor="other">Other</label>
                </FormElement>
                {errors.gender && <Error>{errors.gender.message}</Error>}
            </FormItem>
            <FormItem>
                <Label>Country</Label>
                <FormElement>
                {/* <div className="floatLabelCountry">
                    <FlagIcon src={Flag}></FlagIcon>
                </div> */}
                    {/* <SelectItem className="countrySelect" name="country" ref={register({required:"This field is required"})}>
                        <option>India</option>
                    </SelectItem> */}

                    <ReactFlagsSelect   defaultCountry="IN" onSelect={selectCountry} className="selectCountry " />

                </FormElement>
                {errors.country && <Error>{errors.country.message}</Error>}
            </FormItem>
            <FormItem>
                <Label>State</Label>
                <FormElement>
                    {/* <InputField autoComplete="new-password" name="state" ref={register({required:"This field is required"})} /> */}
<RegionDropdown
    className="selectCountry "
    countryValueType="short"
  country={countryCode}
  value={region}
  onChange={selectRegion} />
                </FormElement>
                {errors.state && <Error>{errors.state.message}</Error>}
            </FormItem>
            <FormItem>
                <Label className="active">Phone</Label>
                <FormElement>
                <div className="floatLabel">
                    {/* <FlagIcon src={Flag}></FlagIcon>
                    <p>+91</p> */}
                    <PhoneInput defaultCountry={countryCode}/>
                    {
                        countryCode && countryList[countryCode].code
                    }
                </div>
                <InputField autoComplete="new-password" className="phoneInput" type="tel" name="phone_number" maxLength="10"  ref={register({required:true, minLength: 7})} />
                </FormElement>
                {errors.phone_number && errors.phone_number.type === "required" && <Error>This field is required</Error>}
                {errors.phone_number && errors.phone_number.type === "minLength" && <Error>Enter valid phone number</Error> }
            </FormItem>
            <BigButton type="submit" value="Next"/>
        </FormContainer>
        <Footer>
        Already have an account?
            <h3>Log in</h3>
        </Footer>
    </Container>
    </>

}

export default PersonalDetails;