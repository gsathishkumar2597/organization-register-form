export const countryList = {
    AF: {
        name: 'Afghanistan',
        code: '+93'
    },
    AL: {
        name: 'Albania',
        code: '+355'
    },
    DZ: {
        name: 'Algeria',
        code: '+213'
    },
    AS: {
        name: 'American Samoa',
        code: '+1-684'
    },
    AD: {
        name: 'Andorra',
        code: '+376'
    },
    AO: {
        name: 'Angola',
        code: '+244'
    },
    AI: {
        name: 'Anguilla',
        code: '+1-264'
    },
    AQ: {
        name: 'Antarctica',
        code: '+672'
    },
    AG: {
        name: 'Antigua and Barbuda',
        code: '+1-268'
    },
    AR: {
        name: 'Argentina',
        code: '+54'
    },
    AM: {
        name: 'Armenia',
        code: '+374'
    },
    AW: {
        name: 'Aruba',
        code: '+297'
    },
    AU: {
        name: 'Australia',
        code: '+61'
    },
    AT: {
        name: 'Austria',
        code: '+43'
    },
    AZ: {
        name: 'Azerbaijan',
        code: '+994'
    },
    BS: {
        name: 'Bahamas',
        code: '+1-242'
    },
    BH: {
        name: 'Bahrain',
        code: '+973'
    },
    BD: {
        name: 'Bangladesh',
        code: '+880'
    },
    BB: {
        name: 'Barbados',
        code: '+1-246'
    },
    BY: {
        name: 'Belarus',
        code: '+375'
    },
    BE: {
        name: 'Belgium',
        code: '+32'
    },
    BZ: {
        name: 'Belize',
        code: '+501'
    },
    BJ: {
        name: 'Benin',
        code: '+229'
    },
    BM: {
        name: 'Bermuda',
        code: '+1-441'
    },
    BT: {
        name: 'Bhutan',
        code: '+975'
    },
    BO: {
        name: 'Bolivia',
        code: '+591'
    },
    BA: {
        name: 'Bosnia and Herzegovina',
        code: '+387'
    },
    BW: {
        name: 'Botswana',
        code: '+267'
    },
    BR: {
        name: 'Brazil',
        code: '+55'
    },
    IO: {
        name: 'British Indian Ocean Territory',
        code: '+246'
    },
    VG: {
        name: 'British Virgin Islands',
        code: '+1-284'
    },
    BN: {
        name: 'Brunei',
        code: '+673'
    },
    BG: {
        name: 'Bulgaria',
        code: '+359'
    },
    BF: {
        name: 'Burkina Faso',
        code: '+226'
    },
    BI: {
        name: 'Burundi',
        code: '+257'
    },
    KH: {
        name: 'Cambodia',
        code: '+855'
    },
    CM: {
        name: 'Cameroon',
        code: '+237'
    },
    CA: {
        name: 'Canada',
        code: '+1'
    },
    CV: {
        name: 'Cape Verde',
        code: '+238'
    },
    KY: {
        name: 'Cayman Islands',
        code: '+1-345'
    },
    CF: {
        name: 'Central African Republic',
        code: '+236'
    },
    TD: {
        name: 'Chad',
        code: '+235'
    },
    CL: {
        name: 'Chile',
        code: '+56'
    },
    CN: {
        name: 'China',
        code: '+86'
    },
    CX: {
        name: 'Christmas Island',
        code: '+61'
    },
    CC: {
        name: 'Cocos Islands',
        code: '+61'
    },
    CO: {
        name: 'Colombia',
        code: '+57'
    },
    KM: {
        name: 'Comoros',
        code: '+269'
    },
    CK: {
        name: 'Cook Islands',
        code: '+682'
    },
    CR: {
        name: 'Costa Rica',
        code: '+506'
    },
    HR: {
        name: 'Croatia',
        code: '+385'
    },
    CU: {
        name: 'Cuba',
        code: '+53'
    },
    CW: {
        name: 'Curacao',
        code: '+599'
    },
    CY: {
        name: 'Cyprus',
        code: '+357'
    },
    CZ: {
        name: 'Czech Republic',
        code: '+420'
    },
    CD: {
        name: 'Democratic Republic of the Congo',
        code: '+243'
    },
    DK: {
        name: 'Denmark',
        code: '+45'
    },
    DJ: {
        name: 'Djibouti',
        code: '+253'
    },
    DM: {
        name: 'Dominica',
        code: '+1-767'
    },
    DO: {
        name: 'Dominican Republic',
        code: '+1-809, 1-829, 1-849'
    },
    TL: {
        name: 'East Timor',
        code: '+670'
    },
    EC: {
        name: 'Ecuador',
        code: '+593'
    },
    EG: {
        name: 'Egypt',
        code: '+20'
    },
    SV: {
        name: 'El Salvador',
        code: '+503'
    },
    GQ: {
        name: 'Equatorial Guinea',
        code: '+240'
    },
    ER: {
        name: 'Eritrea',
        code: '+291'
    },
    EE: {
        name: 'Estonia',
        code: '+372'
    },
    ET: {
        name: 'Ethiopia',
        code: '+251'
    },
    FK: {
        name: 'Falkland Islands',
        code: '+500'
    },
    FO: {
        name: 'Faroe Islands',
        code: '+298'
    },
    FJ: {
        name: 'Fiji',
        code: '+679'
    },
    FI: {
        name: 'Finland',
        code: '+358'
    },
    FR: {
        name: 'France',
        code: '+33'
    },
    PF: {
        name: 'French Polynesia',
        code: '+689'
    },
    GA: {
        name: 'Gabon',
        code: '+241'
    },
    GM: {
        name: 'Gambia',
        code: '+220'
    },
    GE: {
        name: 'Georgia',
        code: '+995'
    },
    DE: {
        name: 'Germany',
        code: '+49'
    },
    GH: {
        name: 'Ghana',
        code: '+233'
    },
    GI: {
        name: 'Gibraltar',
        code: '+350'
    },
    GR: {
        name: 'Greece',
        code: '+30'
    },
    GL: {
        name: 'Greenland',
        code: '+299'
    },
    GD: {
        name: 'Grenada',
        code: '+1-473'
    },
    GU: {
        name: 'Guam',
        code: '+1-671'
    },
    GT: {
        name: 'Guatemala',
        code: '+502'
    },
    GG: {
        name: 'Guernsey',
        code: '+44-1481'
    },
    GN: {
        name: 'Guinea',
        code: '+224'
    },
    GW: {
        name: 'Guinea-Bissau',
        code: '+245'
    },
    GY: {
        name: 'Guyana',
        code: '+592'
    },
    HT: {
        name: 'Haiti',
        code: '+509'
    },
    HN: {
        name: 'Honduras',
        code: '+504'
    },
    HK: {
        name: 'Hong Kong',
        code: '+852'
    },
    HU: {
        name: 'Hungary',
        code: '+36'
    },
    IS: {
        name: 'Iceland',
        code: '+354'
    },
    IN: {
        name: 'India',
        code: '+91'
    },
    ID: {
        name: 'Indonesia',
        code: '+62'
    },
    IR: {
        name: 'Iran',
        code: '+98'
    },
    IQ: {
        name: 'Iraq',
        code: '+964'
    },
    IE: {
        name: 'Ireland',
        code: '+353'
    },
    IM: {
        name: 'Isle of Man',
        code: '+44-1624'
    },
    IL: {
        name: 'Israel',
        code: '+972'
    },
    IT: {
        name: 'Italy',
        code: '+39'
    },
    CI: {
        name: 'Ivory Coast',
        code: '+225'
    },
    JM: {
        name: 'Jamaica',
        code: '+1-876'
    },
    JP: {
        name: 'Japan',
        code: '+81'
    },
    JE: {
        name: 'Jersey',
        code: '+44-1534'
    },
    JO: {
        name: 'Jordan',
        code: '+962'
    },
    KZ: {
        name: 'Kazakhstan',
        code: '+7'
    },
    KE: {
        name: 'Kenya',
        code: '+254'
    },
    KI: {
        name: 'Kiribati',
        code: '+686'
    },
    XK: {
        name: 'Kosovo',
        code: '+383'
    },
    KW: {
        name: 'Kuwait',
        code: '+965'
    },
    KG: {
        name: 'Kyrgyzstan',
        code: '+996'
    },
    LA: {
        name: 'Laos',
        code: '+856'
    },
    LV: {
        name: 'Latvia',
        code: '+371'
    },
    LB: {
        name: 'Lebanon',
        code: '+961'
    },
    LS: {
        name: 'Lesotho',
        code: '+266'
    },
    LR: {
        name: 'Liberia',
        code: '+231'
    },
    LY: {
        name: 'Libya',
        code: '+218'
    },
    LI: {
        name: 'Liechtenstein',
        code: '+423'
    },
    LT: {
        name: 'Lithuania',
        code: '+370'
    },
    LU: {
        name: 'Luxembourg',
        code: '+352'
    },
    MO: {
        name: 'Macau',
        code: '+853'
    },
    MK: {
        name: 'Macedonia',
        code: '+389'
    },
    MG: {
        name: 'Madagascar',
        code: '+261'
    },
    MW: {
        name: 'Malawi',
        code: '+265'
    },
    MY: {
        name: 'Malaysia',
        code: '+60'
    },
    MV: {
        name: 'Maldives',
        code: '+960'
    },
    ML: {
        name: 'Mali',
        code: '+223'
    },
    MT: {
        name: 'Malta',
        code: '+356'
    },
    MH: {
        name: 'Marshall Islands',
        code: '+692'
    },
    MR: {
        name: 'Mauritania',
        code: '+222'
    },
    MU: {
        name: 'Mauritius',
        code: '+230'
    },
    YT: {
        name: 'Mayotte',
        code: '+262'
    },
    MX: {
        name: 'Mexico',
        code: '+52'
    },
    FM: {
        name: 'Micronesia',
        code: '+691'
    },
    MD: {
        name: 'Moldova',
        code: '+373'
    },
    MC: {
        name: 'Monaco',
        code: '+377'
    },
    MN: {
        name: 'Mongolia',
        code: '+976'
    },
    ME: {
        name: 'Montenegro',
        code: '+382'
    },
    MS: {
        name: 'Montserrat',
        code: '+1-664'
    },
    MA: {
        name: 'Morocco',
        code: '+212'
    },
    MZ: {
        name: 'Mozambique',
        code: '+258'
    },
    MM: {
        name: 'Myanmar',
        code: '+95'
    },
    NA: {
        name: 'Namibia',
        code: '+264'
    },
    NR: {
        name: 'Nauru',
        code: '+674'
    },
    NP: {
        name: 'Nepal',
        code: '+977'
    },
    NL: {
        name: 'Netherlands',
        code: '+31'
    },
    AN: {
        name: 'Netherlands Antilles',
        code: '+599'
    },
    NC: {
        name: 'New Caledonia',
        code: '+687'
    },
    NZ: {
        name: 'New Zealand',
        code: '+64'
    },
    NI: {
        name: 'Nicaragua',
        code: '+505'
    },
    NE: {
        name: 'Niger',
        code: '+227'
    },
    NG: {
        name: 'Nigeria',
        code: '+234'
    },
    NU: {
        name: 'Niue',
        code: '+683'
    },
    KP: {
        name: 'North Korea',
        code: '+850'
    },
    MP: {
        name: 'Northern Mariana Islands',
        code: '+1-670'
    },
    NO: {
        name: 'Norway',
        code: '+47'
    },
    OM: {
        name: 'Oman',
        code: '+968'
    },
    PK: {
        name: 'Pakistan',
        code: '+92'
    },
    PW: {
        name: 'Palau',
        code: '+680'
    },
    PS: {
        name: 'Palestine',
        code: '+970'
    },
    PA: {
        name: 'Panama',
        code: '+507'
    },
    PG: {
        name: 'Papua New Guinea',
        code: '+675'
    },
    PY: {
        name: 'Paraguay',
        code: '+595'
    },
    PE: {
        name: 'Peru',
        code: '+51'
    },
    PH: {
        name: 'Philippines',
        code: '+63'
    },
    PN: {
        name: 'Pitcairn',
        code: '+64'
    },
    PL: {
        name: 'Poland',
        code: '+48'
    },
    PT: {
        name: 'Portugal',
        code: '+351'
    },
    PR: {
        name: 'Puerto Rico',
        code: '+1-787, 1-939'
    },
    QA: {
        name: 'Qatar',
        code: '+974'
    },
    CG: {
        name: 'Republic of the Congo',
        code: '+242'
    },
    RE: {
        name: 'Reunion',
        code: '+262'
    },
    RO: {
        name: 'Romania',
        code: '+40'
    },
    RU: {
        name: 'Russia',
        code: '+7'
    },
    RW: {
        name: 'Rwanda',
        code: '+250'
    },
    BL: {
        name: 'Saint Barthelemy',
        code: '+590'
    },
    SH: {
        name: 'Saint Helena',
        code: '+290'
    },
    KN: {
        name: 'Saint Kitts and Nevis',
        code: '+1-869'
    },
    LC: {
        name: 'Saint Lucia',
        code: '+1-758'
    },
    MF: {
        name: 'Saint Martin',
        code: '+590'
    },
    PM: {
        name: 'Saint Pierre and Miquelon',
        code: '+508'
    },
    VC: {
        name: 'Saint Vincent and the Grenadines',
        code: '+1-784'
    },
    WS: {
        name: 'Samoa',
        code: '+685'
    },
    SM: {
        name: 'San Marino',
        code: '+378'
    },
    ST: {
        name: 'Sao Tome and Principe',
        code: '+239'
    },
    SA: {
        name: 'Saudi Arabia',
        code: '+966'
    },
    SN: {
        name: 'Senegal',
        code: '+221'
    },
    RS: {
        name: 'Serbia',
        code: '+381'
    },
    SC: {
        name: 'Seychelles',
        code: '+248'
    },
    SL: {
        name: 'Sierra Leone',
        code: '+232'
    },
    SG: {
        name: 'Singapore',
        code: '+65'
    },
    SX: {
        name: 'Sint Maarten',
        code: '+1-721'
    },
    SK: {
        name: 'Slovakia',
        code: '+421'
    },
    SI: {
        name: 'Slovenia',
        code: '+386'
    },
    SB: {
        name: 'Solomon Islands',
        code: '+677'
    },
    SO: {
        name: 'Somalia',
        code: '+252'
    },
    ZA: {
        name: 'South Africa',
        code: '+27'
    },
    KR: {
        name: 'South Korea',
        code: '+82'
    },
    SS: {
        name: 'South Sudan',
        code: '+211'
    },
    ES: {
        name: 'Spain',
        code: '+34'
    },
    LK: {
        name: 'Sri Lanka',
        code: '+94'
    },
    SD: {
        name: 'Sudan',
        code: '+249'
    },
    SR: {
        name: 'Suriname',
        code: '+597'
    },
    SJ: {
        name: 'Svalbard and Jan Mayen',
        code: '+47'
    },
    SZ: {
        name: 'Swaziland',
        code: '+268'
    },
    SE: {
        name: 'Sweden',
        code: '+46'
    },
    CH: {
        name: 'Switzerland',
        code: '+41'
    },
    SY: {
        name: 'Syria',
        code: '+963'
    },
    TW: {
        name: 'Taiwan',
        code: '+886'
    },
    TJ: {
        name: 'Tajikistan',
        code: '+992'
    },
    TZ: {
        name: 'Tanzania',
        code: '+255'
    },
    TH: {
        name: 'Thailand',
        code: '+66'
    },
    TG: {
        name: 'Togo',
        code: '+228'
    },
    TK: {
        name: 'Tokelau',
        code: '+690'
    },
    TO: {
        name: 'Tonga',
        code: '+676'
    },
    TT: {
        name: 'Trinidad and Tobago',
        code: '+1-868'
    },
    TN: {
        name: 'Tunisia',
        code: '+216'
    },
    TR: {
        name: 'Turkey',
        code: '+90'
    },
    TM: {
        name: 'Turkmenistan',
        code: '+993'
    },
    TC: {
        name: 'Turks and Caicos Islands',
        code: '+1-649'
    },
    TV: {
        name: 'Tuvalu',
        code: '+688'
    },
    VI: {
        name: 'U.S. Virgin Islands',
        code: '+1-340'
    },
    UG: {
        name: 'Uganda',
        code: '+256'
    },
    UA: {
        name: 'Ukraine',
        code: '+380'
    },
    AE: {
        name: 'United Arab Emirates',
        code: '+971'
    },
    GB: {
        name: 'United Kingdom',
        code: '+44'
    },
    US: {
        name: 'United States',
        code: '+1'
    },
    UY: {
        name: 'Uruguay',
        code: '+598'
    },
    UZ: {
        name: 'Uzbekistan',
        code: '+998'
    },
    VU: {
        name: 'Vanuatu',
        code: '+678'
    },
    VA: {
        name: 'Vatican',
        code: '+379'
    },
    VE: {
        name: 'Venezuela',
        code: '+58'
    },
    VN: {
        name: 'Vietnam',
        code: '+84'
    },
    WF: {
        name: 'Wallis and Futuna',
        code: '+681'
    },
    EH: {
        name: 'Western Sahara',
        code: '+212'
    },
    YE: {
        name: 'Yemen',
        code: '+967'
    },
    ZM: {
        name: 'Zambia',
        code: '+260'
    },
    ZW:{
        name: 'Zimbabwe',
        code: '+263'
    }
}


export const country_list = [ 
  {name: 'Select Your Country', code: ' '},
  {name: 'Afghanistan', code: 'AF'}, 
  {name: 'Åland Islands', code: 'AX'}, 
  {name: 'Albania', code: 'AL'}, 
  {name: 'Algeria', code: 'DZ'}, 
  {name: 'American Samoa', code: 'AS'}, 
  {name: 'AndorrA', code: 'AD'}, 
  {name: 'Angola', code: 'AO'}, 
  {name: 'Anguilla', code: 'AI'}, 
  {name: 'Antarctica', code: 'AQ'}, 
  {name: 'Antigua and Barbuda', code: 'AG'}, 
  {name: 'Argentina', code: 'AR'}, 
  {name: 'Armenia', code: 'AM'}, 
  {name: 'Aruba', code: 'AW'}, 
  {name: 'Australia', code: 'AU'}, 
  {name: 'Austria', code: 'AT'}, 
  {name: 'Azerbaijan', code: 'AZ'}, 
  {name: 'Bahamas', code: 'BS'}, 
  {name: 'Bahrain', code: 'BH'}, 
  {name: 'Bangladesh', code: 'BD'}, 
  {name: 'Barbados', code: 'BB'}, 
  {name: 'Belarus', code: 'BY'}, 
  {name: 'Belgium', code: 'BE'}, 
  {name: 'Belize', code: 'BZ'}, 
  {name: 'Benin', code: 'BJ'}, 
  {name: 'Bermuda', code: 'BM'}, 
  {name: 'Bhutan', code: 'BT'}, 
  {name: 'Bolivia', code: 'BO'}, 
  {name: 'Bosnia and Herzegovina', code: 'BA'}, 
  {name: 'Botswana', code: 'BW'}, 
  {name: 'Bouvet Island', code: 'BV'}, 
  {name: 'Brazil', code: 'BR'}, 
  {name: 'British Indian Ocean Territory', code: 'IO'}, 
  {name: 'Brunei Darussalam', code: 'BN'}, 
  {name: 'Bulgaria', code: 'BG'}, 
  {name: 'Burkina Faso', code: 'BF'}, 
  {name: 'Burundi', code: 'BI'}, 
  {name: 'Cambodia', code: 'KH'}, 
  {name: 'Cameroon', code: 'CM'}, 
  {name: 'Canada', code: 'CA'}, 
  {name: 'Cape Verde', code: 'CV'}, 
  {name: 'Cayman Islands', code: 'KY'}, 
  {name: 'Central African Republic', code: 'CF'}, 
  {name: 'Chad', code: 'TD'}, 
  {name: 'Chile', code: 'CL'}, 
  {name: 'China', code: 'CN'}, 
  {name: 'Christmas Island', code: 'CX'}, 
  {name: 'Cocos (Keeling) Islands', code: 'CC'}, 
  {name: 'Colombia', code: 'CO'}, 
  {name: 'Comoros', code: 'KM'}, 
  {name: 'Congo', code: 'CG'}, 
  {name: 'Congo, The Democratic Republic of the', code: 'CD'}, 
  {name: 'Cook Islands', code: 'CK'}, 
  {name: 'Costa Rica', code: 'CR'}, 
  {name: 'Cote D\'Ivoire', code: 'CI'}, 
  {name: 'Croatia', code: 'HR'}, 
  {name: 'Cuba', code: 'CU'}, 
  {name: 'Cyprus', code: 'CY'}, 
  {name: 'Czech Republic', code: 'CZ'}, 
  {name: 'Denmark', code: 'DK'}, 
  {name: 'Djibouti', code: 'DJ'}, 
  {name: 'Dominica', code: 'DM'}, 
  {name: 'Dominican Republic', code: 'DO'}, 
  {name: 'Ecuador', code: 'EC'}, 
  {name: 'Egypt', code: 'EG'}, 
  {name: 'El Salvador', code: 'SV'}, 
  {name: 'Equatorial Guinea', code: 'GQ'}, 
  {name: 'Eritrea', code: 'ER'}, 
  {name: 'Estonia', code: 'EE'}, 
  {name: 'Ethiopia', code: 'ET'}, 
  {name: 'Falkland Islands (Malvinas)', code: 'FK'}, 
  {name: 'Faroe Islands', code: 'FO'}, 
  {name: 'Fiji', code: 'FJ'}, 
  {name: 'Finland', code: 'FI'}, 
  {name: 'France', code: 'FR'}, 
  {name: 'French Guiana', code: 'GF'}, 
  {name: 'French Polynesia', code: 'PF'}, 
  {name: 'French Southern Territories', code: 'TF'}, 
  {name: 'Gabon', code: 'GA'}, 
  {name: 'Gambia', code: 'GM'}, 
  {name: 'Georgia', code: 'GE'}, 
  {name: 'Germany', code: 'DE'}, 
  {name: 'Ghana', code: 'GH'}, 
  {name: 'Gibraltar', code: 'GI'}, 
  {name: 'Greece', code: 'GR'}, 
  {name: 'Greenland', code: 'GL'}, 
  {name: 'Grenada', code: 'GD'}, 
  {name: 'Guadeloupe', code: 'GP'}, 
  {name: 'Guam', code: 'GU'}, 
  {name: 'Guatemala', code: 'GT'}, 
  {name: 'Guernsey', code: 'GG'}, 
  {name: 'Guinea', code: 'GN'}, 
  {name: 'Guinea-Bissau', code: 'GW'}, 
  {name: 'Guyana', code: 'GY'}, 
  {name: 'Haiti', code: 'HT'}, 
  {name: 'Heard Island and Mcdonald Islands', code: 'HM'}, 
  {name: 'Holy See (Vatican City State)', code: 'VA'}, 
  {name: 'Honduras', code: 'HN'}, 
  {name: 'Hong Kong', code: 'HK'}, 
  {name: 'Hungary', code: 'HU'}, 
  {name: 'Iceland', code: 'IS'}, 
  {name: 'India', code: 'IN'}, 
  {name: 'Indonesia', code: 'ID'}, 
  {name: 'Iran, Islamic Republic Of', code: 'IR'}, 
  {name: 'Iraq', code: 'IQ'}, 
  {name: 'Ireland', code: 'IE'}, 
  {name: 'Isle of Man', code: 'IM'}, 
  {name: 'Israel', code: 'IL'}, 
  {name: 'Italy', code: 'IT'}, 
  {name: 'Jamaica', code: 'JM'}, 
  {name: 'Japan', code: 'JP'}, 
  {name: 'Jersey', code: 'JE'}, 
  {name: 'Jordan', code: 'JO'}, 
  {name: 'Kazakhstan', code: 'KZ'}, 
  {name: 'Kenya', code: 'KE'}, 
  {name: 'Kiribati', code: 'KI'}, 
  {name: 'Korea, Democratic People\'S Republic of', code: 'KP'}, 
  {name: 'Korea, Republic of', code: 'KR'}, 
  {name: 'Kuwait', code: 'KW'}, 
  {name: 'Kyrgyzstan', code: 'KG'}, 
  {name: 'Lao People\'S Democratic Republic', code: 'LA'}, 
  {name: 'Latvia', code: 'LV'}, 
  {name: 'Lebanon', code: 'LB'}, 
  {name: 'Lesotho', code: 'LS'}, 
  {name: 'Liberia', code: 'LR'}, 
  {name: 'Libyan Arab Jamahiriya', code: 'LY'}, 
  {name: 'Liechtenstein', code: 'LI'}, 
  {name: 'Lithuania', code: 'LT'}, 
  {name: 'Luxembourg', code: 'LU'}, 
  {name: 'Macao', code: 'MO'}, 
  {name: 'Macedonia, The Former Yugoslav Republic of', code: 'MK'}, 
  {name: 'Madagascar', code: 'MG'}, 
  {name: 'Malawi', code: 'MW'}, 
  {name: 'Malaysia', code: 'MY'}, 
  {name: 'Maldives', code: 'MV'}, 
  {name: 'Mali', code: 'ML'}, 
  {name: 'Malta', code: 'MT'}, 
  {name: 'Marshall Islands', code: 'MH'}, 
  {name: 'Martinique', code: 'MQ'}, 
  {name: 'Mauritania', code: 'MR'}, 
  {name: 'Mauritius', code: 'MU'}, 
  {name: 'Mayotte', code: 'YT'}, 
  {name: 'Mexico', code: 'MX'}, 
  {name: 'Micronesia, Federated States of', code: 'FM'}, 
  {name: 'Moldova, Republic of', code: 'MD'}, 
  {name: 'Monaco', code: 'MC'}, 
  {name: 'Mongolia', code: 'MN'}, 
  {name: 'Montserrat', code: 'MS'}, 
  {name: 'Morocco', code: 'MA'}, 
  {name: 'Mozambique', code: 'MZ'}, 
  {name: 'Myanmar', code: 'MM'}, 
  {name: 'Namibia', code: 'NA'}, 
  {name: 'Nauru', code: 'NR'}, 
  {name: 'Nepal', code: 'NP'}, 
  {name: 'Netherlands', code: 'NL'}, 
  {name: 'Netherlands Antilles', code: 'AN'}, 
  {name: 'New Caledonia', code: 'NC'}, 
  {name: 'New Zealand', code: 'NZ'}, 
  {name: 'Nicaragua', code: 'NI'}, 
  {name: 'Niger', code: 'NE'}, 
  {name: 'Nigeria', code: 'NG'}, 
  {name: 'Niue', code: 'NU'}, 
  {name: 'Norfolk Island', code: 'NF'}, 
  {name: 'Northern Mariana Islands', code: 'MP'}, 
  {name: 'Norway', code: 'NO'}, 
  {name: 'Oman', code: 'OM'}, 
  {name: 'Pakistan', code: 'PK'}, 
  {name: 'Palau', code: 'PW'}, 
  {name: 'Palestinian Territory, Occupied', code: 'PS'}, 
  {name: 'Panama', code: 'PA'}, 
  {name: 'Papua New Guinea', code: 'PG'}, 
  {name: 'Paraguay', code: 'PY'}, 
  {name: 'Peru', code: 'PE'}, 
  {name: 'Philippines', code: 'PH'}, 
  {name: 'Pitcairn', code: 'PN'}, 
  {name: 'Poland', code: 'PL'}, 
  {name: 'Portugal', code: 'PT'}, 
  {name: 'Puerto Rico', code: 'PR'}, 
  {name: 'Qatar', code: 'QA'}, 
  {name: 'Reunion', code: 'RE'}, 
  {name: 'Romania', code: 'RO'}, 
  {name: 'Russian Federation', code: 'RU'}, 
  {name: 'RWANDA', code: 'RW'}, 
  {name: 'Saint Helena', code: 'SH'}, 
  {name: 'Saint Kitts and Nevis', code: 'KN'}, 
  {name: 'Saint Lucia', code: 'LC'}, 
  {name: 'Saint Pierre and Miquelon', code: 'PM'}, 
  {name: 'Saint Vincent and the Grenadines', code: 'VC'}, 
  {name: 'Samoa', code: 'WS'}, 
  {name: 'San Marino', code: 'SM'}, 
  {name: 'Sao Tome and Principe', code: 'ST'}, 
  {name: 'Saudi Arabia', code: 'SA'}, 
  {name: 'Senegal', code: 'SN'}, 
  {name: 'Serbia and Montenegro', code: 'CS'}, 
  {name: 'Seychelles', code: 'SC'}, 
  {name: 'Sierra Leone', code: 'SL'}, 
  {name: 'Singapore', code: 'SG'}, 
  {name: 'Slovakia', code: 'SK'}, 
  {name: 'Slovenia', code: 'SI'}, 
  {name: 'Solomon Islands', code: 'SB'}, 
  {name: 'Somalia', code: 'SO'}, 
  {name: 'South Africa', code: 'ZA'}, 
  {name: 'South Georgia and the South Sandwich Islands', code: 'GS'}, 
  {name: 'Spain', code: 'ES'}, 
  {name: 'Sri Lanka', code: 'LK'}, 
  {name: 'Sudan', code: 'SD'}, 
  {name: 'Suriname', code: 'SR'}, 
  {name: 'Svalbard and Jan Mayen', code: 'SJ'}, 
  {name: 'Swaziland', code: 'SZ'}, 
  {name: 'Sweden', code: 'SE'}, 
  {name: 'Switzerland', code: 'CH'}, 
  {name: 'Syrian Arab Republic', code: 'SY'}, 
  {name: 'Taiwan, Province of China', code: 'TW'}, 
  {name: 'Tajikistan', code: 'TJ'}, 
  {name: 'Tanzania, United Republic of', code: 'TZ'}, 
  {name: 'Thailand', code: 'TH'}, 
  {name: 'Timor-Leste', code: 'TL'}, 
  {name: 'Togo', code: 'TG'}, 
  {name: 'Tokelau', code: 'TK'}, 
  {name: 'Tonga', code: 'TO'}, 
  {name: 'Trinidad and Tobago', code: 'TT'}, 
  {name: 'Tunisia', code: 'TN'}, 
  {name: 'Turkey', code: 'TR'}, 
  {name: 'Turkmenistan', code: 'TM'}, 
  {name: 'Turks and Caicos Islands', code: 'TC'}, 
  {name: 'Tuvalu', code: 'TV'}, 
  {name: 'Uganda', code: 'UG'}, 
  {name: 'Ukraine', code: 'UA'}, 
  {name: 'United Arab Emirates', code: 'AE'}, 
  {name: 'United Kingdom', code: 'GB'}, 
  {name: 'United States', code: 'US'}, 
  {name: 'United States Minor Outlying Islands', code: 'UM'}, 
  {name: 'Uruguay', code: 'UY'}, 
  {name: 'Uzbekistan', code: 'UZ'}, 
  {name: 'Vanuatu', code: 'VU'}, 
  {name: 'Venezuela', code: 'VE'}, 
  {name: 'Viet Nam', code: 'VN'}, 
  {name: 'Virgin Islands, British', code: 'VG'}, 
  {name: 'Virgin Islands, U.S.', code: 'VI'}, 
  {name: 'Wallis and Futuna', code: 'WF'}, 
  {name: 'Western Sahara', code: 'EH'}, 
  {name: 'Yemen', code: 'YE'}, 
  {name: 'Zambia', code: 'ZM'}, 
  {name: 'Zimbabwe', code: 'ZW'} 
]

export function getCountryName(code){
    return country_list.map(c=>{
        if(c.code === code){
            console.log(c.name);
            return c.name
           
        }
    })
}