// NPM IMPORTS
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from "react-hook-form";

//COMPONENTS IMPORT
import NavBar from "../NavBar/Navbar";

//MEDIA
import ImagePlcaeHolder from "../../../media/placeholder.svg"

//STYLES
import {Container, Heading, FormContainer, Image, LogoLabel, FormItem, Label, InputField, Terms, CheckBox, Text, Button, Back, OTP, Error } from "./CompanyDetails.styled";

var bool = false;

const CompanyDetails = () =>{
    const history= useHistory();

    const [year,setYear] = useState('');
    const [image,setImage] = useState('');
    const [isImage,setIsImage] = useState(false)
    const [imageErr,setImageErr] = useState(true)

    
    const goBack = () =>{
        history.push("/");
    }

    const { register, handleSubmit, errors } = useForm();

    const handleChange = (event) => {
        const re = /^[0-9\b]+$/;
          if (event.target.value === '' || re.test(event.target.value)) {
                setYear(event.target.value);
          }
      }

    const onsubmit = (data) =>{
        if(!image){
            setImageErr(false)
        }
        if(data && image){
            data.logo_image = image;
            history.push("/email_verification");
            localStorage.setItem("isValidCompanyDetails",JSON.stringify(!bool));
            localStorage.setItem("CompanyDetails",JSON.stringify(data));
        }
    }

    const onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
          let reader = new FileReader();
          reader.onload = (e) => {
            setImageErr(true)
            setIsImage(true)
            setImage(e.target.result);
          };
          reader.readAsDataURL(event.target.files[0]);
        }
      }

    useEffect(()=>{
        localStorage.setItem("isValidCompanyDetails",JSON.stringify(bool));
        if(!JSON.parse(localStorage.getItem("isValidPersonalDetails")))
        history.push("/");
    });
    return <>
    <NavBar active="CompanyDetails" completed="PersonalDetails"/>
    <Container>
        <Heading>
            <h2>Add your company details</h2>
            <h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3> 
        </Heading>
        <FormContainer method="post" onSubmit={handleSubmit(onsubmit)}>
            <Image>
                <img src={image ? image : ImagePlcaeHolder}/>
                {
                    !isImage ?
                    <LogoLabel><label for="file-upload" style={{cursor:"pointer"}} className="filetype">Upload your company logo</label><input accept="image/x-png,image/gif,image/jpeg" style={{display:"none"}} type="file" id="file-upload" onChange={onImageChange} className="filetype"/></LogoLabel>
                    :
                    <LogoLabel><label for="file-upload" style={{cursor:"pointer"}} className="filetype">Change your company logo</label><input accept="image/x-png,image/gif,image/jpeg" style={{display:"none"}} type="file" id="file-upload" onChange={onImageChange} className="filetype"/></LogoLabel>
                }

                
            </Image>
            <FormItem>
                <Label>Company Name</Label>
                <InputField type="text" name="company_name" ref={register({required:"This field is required"})} />
                {errors.company_name && <Error>{errors.company_name.message}</Error>}
            </FormItem>
            <FormItem>
                <Label>Email id</Label>
                <InputField type="text" name="email_id" ref={register({
                        required: "This field is required",
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: "Invalid email address"
                                }   })} />
                {errors.email_id && <Error>{errors.email_id.message}</Error>}
            </FormItem>
            <FormItem>
                <Label>Job Title</Label>
                <InputField  type="text" name="job_title" ref={register({required:"This field is required"})} />
                {errors.job_title && <Error>{errors.job_title.message}</Error>}
            </FormItem>
            <FormItem>
                <Label>Years of Experience</Label>
                <InputField value={year} onChange={handleChange} maxLength="2"   type="text" name="years" ref={register({required:true, maxLength: 3})} />
                {errors.years && errors.years.type === "required" && <Error>This field is required</Error>}
            </FormItem>
            <Terms>
                <CheckBox type="checkbox" name="accept_terms" ref={register({required:true})}></CheckBox>
                <Text>
                I accept the
                <b>Terms and Conditions</b>
                </Text>          
            </Terms>
            <br/>
                {errors.accept_terms && errors.accept_terms.type === "required" && <Error style={{marginLeft:32}}>Please read and accept Terms & Conditions</Error>} 
                {!imageErr && <Error style={{marginLeft:32}}>Please select a Company logo</Error>}  
            <Button>
                <Back onClick={goBack}>Back</Back>
                <OTP>Send OTP</OTP>
            </Button>
        </FormContainer>
    </Container>
    </>

}

export default CompanyDetails;