// NPM IMPORTS
import React from 'react';

//COMPONENTS IMPORT

//STYLES
import { Container, NavLinks, Numbering, NavLink, Title } from "./Navbar.styled";

//MEDIA
import Tick from "../../../media/tick.svg"

const NavBar = (props) =>{
    const  active= props.active;
    return <Container>
        <NavLinks>
            <NavLink>
                {
                    props.completed === "PersonalDetails" || props.completed === "CompanyDetails" ? 
                    <Numbering ><img src={Tick} alt="tick"/></Numbering> :
                    <Numbering className={active==="PersonalDetails"? "active" : null}>1</Numbering>
                }
            <Title className={active==="PersonalDetails"? "activeTitle" : null}> Personal Details </Title>
            </NavLink>
            <NavLink>
            {
                    props.completed === "CompanyDetails"? 
                    <Numbering ><img src={Tick} alt="tick"/></Numbering> :
                    <Numbering className={active==="CompanyDetails"? "active" : null}>2</Numbering>
            }
            <Title className={active==="CompanyDetails"? "activeTitle" : null}> Company Details </Title>
            </NavLink>
            <NavLink>
            <Numbering className={active==="EmailVerification"? "active" : null}>3</Numbering>
            <Title className={active==="EmailVerification"? "activeTitle" : null}> Email Verification </Title>
            </NavLink>
        </NavLinks>
    </Container>
}

export default NavBar;