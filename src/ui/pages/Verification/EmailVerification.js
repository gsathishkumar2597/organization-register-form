// NPM IMPORTS
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import OtpInput from "react-otp-input";

//COMPONENTS IMPORT
import NavBar from "../NavBar/Navbar";

//MEDIA

//STYLES
import {
  Container,
  Heading,
  FormContainer,
  FormItem,
  Label,
  InputField,
  Button,
  OTP,
  Back,
  Info,
} from "./EmailVerification.styled";

const EmailVerification = () => {
  const history = useHistory();

  const [otp, setotp] = useState();
  const [error,setError] = useState(false);

  const goBack = () => {
    history.push("/company_details");
  };

  const { register, handleSubmit, errors } = useForm();

  useEffect(() => {
    if (!JSON.parse(localStorage.getItem("isValidCompanyDetails")))
      history.push("/");
  });

  const onSubmit = () => {
    if(otp)
    {
        if (otp.length>3) {
            setError(false)
            localStorage.setItem("OTP", JSON.stringify(otp));
            history.push("/success");
          }
          else{
              setError(true)
          }
    }
    else{
        setError(true)

      }
  };

  const handleChange = (value) =>{
        setotp(value)
}

  return (
    <>
      <NavBar active="EmailVerification" completed="CompanyDetails" />
      <Container>
        <Heading>
          <h2>Enter your OTP</h2>
          <h3>
            For your security, we need to verify your identity. We sent a
            5-digit code to <b>name@domain.com.</b> Please enter it below.
          </h3>
        </Heading>
        <FormContainer>
          <FormItem>
            <Label>Enter your code</Label>
            <div>
              {/* <InputField name="index1" ref={register({required:true})} maxLength="1"/>
                <InputField name="index2" ref={register({required:true})} maxLength="1"/>
                <InputField name="index3" ref={register({required:true})} maxLength="1"/>
                <InputField name="index4" ref={register({required:true})} maxLength="1"/>
                <InputField name="index5" ref={register({required:true})} maxLength="1"/> */}
              <OtpInput
                inputStyle = "input-field"
                value={otp}
                onChange={handleChange}
                numInputs={4}
              />
            </div>
          </FormItem>
          {
                  error &&
                  <b>Please fill the required fields!</b>
                }
          <Button>
            <Back onClick={goBack}>Back</Back>
            <OTP onClick={()=>onSubmit()}>Verify</OTP>
          </Button>
          <hr></hr>
          <Info>
            <h3>
              Didn’t receive the email? Check your spam filter for an email from{" "}
              <b>name@domain.com</b>
            </h3>
          </Info>
        </FormContainer>
      </Container>
    </>
  );
};

export default EmailVerification;
